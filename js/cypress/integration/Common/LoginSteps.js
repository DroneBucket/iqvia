import { Given,When,Then } from "cypress-cucumber-preprocessor/steps";

const url = 'http://sprinkle-burn.glitch.me'
//const welcomeMessage = 'Welcome Dr I Test'
Given('the user has the correct credentials', () => {
  cy.visit(Cypress.config().baseUrl);
});
Given('the user has the incorrect credentials', () => {
  cy.visit(url);
});
And("the user enters username {string}", (uname) => {
  cy.wait(2000)  
  cy.get('input[name="email"]').type(uname)  
});

And("the user enters password {string}", (password) => {
  cy.wait(2000)  
  cy.get('input[name="password"]').type(password)  
});

When("clicks Login", () => {
  cy.get('button[aria-label="login"]').click()
});

Then("the user is presented with a welcome message {string}", (welcomeMessage) => {
 
  cy.contains(welcomeMessage)
});

Then("the user is presented with a error message {string}", (errorMessage) => {
   cy.get('div[id="login-error-box"]').should('have.text',errorMessage)
});
